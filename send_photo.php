<!DOCTYPE html>
<html>
<head>
    <title>Отправка фото</title>
</head>
<body>
<form method="POST" action="./send_photo.php" enctype="multipart/form-data">
    <label for="photo">Выберите фото:</label>
    <input type="file" id="photo" name="photo">
    <input type="submit" value="Загрузить">
</form>

<?php

 session_start();
 if(isset($_SESSION['successcount']) && $_SESSION['successcount'] >= 1) {
  echo "Ошибка: Слишком много загрузок!";
  exit();
 }

 if(isset($_FILES['photo']) && !empty($_FILES['photo']['name'])) {
  $allowedextensions = array('jpg', 'png');
  $allowedsize = 2 * 1024 * 1024; // 2 MB

  try {
   $extension = strtolower(pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION));
   if(!in_array($extension, $allowedextensions)) {
    throw new Exception("Ошибка: Неправильное расширение файла!");
   }

   if($_FILES['photo']['size'] > $allowedsize) {
    throw new Exception("Ошибка: Превышен размер файла!");
   }

   $newfilename = uniqid() . '.' . $extension;
   $destination = 'images/' . $newfilename;

   if(move_uploaded_file($_FILES['photo']['tmp_name'], $destination)) {
    if(isset($_SESSION['successcount'])) {
     $_SESSION['successcount']++;
    } else {
     $_SESSION['successcount'] = 1;
    }
    header("Location: $destination");
    exit();
   } else {
    throw new Exception("Ошибка загрузки файла!");
   }
  } catch(Exception $e) {
   echo $e->getMessage();
  }
 }
 ?>
</body>
</html>